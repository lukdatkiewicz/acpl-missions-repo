## ACPL Missions Repository

Mission repository of Polish ARMA 3 milsim group ACPL

---

** Repository structure

Repository is divided into 3 branches:

1. **MASTER** - missions *.pbo files in at lest beta state, ready to play.
    Commit draft:
        **Author**
         - mission name (mission.pbo) - changes or at least version

2. **DEVELOPMENT** - missions folders in which changes will be made
    Commit draft:
        **Author**
         - mission name (mission folder) - changes
         - mission name (mission folder) - **Another Author** changes to made by someone else who agreed to do them

3. **TEMPLATES** - folders with short templates to use in missions 
    Commit draft:
        **Author**
         - template name (template folder) - short description of template

---

## How to start working with repo

1. go to C:\Users\Username\AppData\Local\Arma 3
2. move /missions content to temporary location
3. git clone https://lukdatkiewicz@bitbucket.org/lukdatkiewicz/acpl-missions-repo.git missions
4. restore missions into /mission folder